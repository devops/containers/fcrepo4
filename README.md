# Fcrepo4 Container Image

## Environment Variables

	FCREPO_BINARY_DIRECTORY=/data/fcrepo.binary.directory
	FCREPO_DB_HOST=db
	FCREPO_DB_NAME=fcrepo
	FCREPO_DB_PASSWORD=fcrepo
	FCREPO_DB_PORT=5432
	FCREPO_DB_USER=fcrepo
	FCREPO_HOME=/data/fcrepo.home
