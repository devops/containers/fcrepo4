#!/bin/bash

wait-for-it "${FCREPO_DB_HOST}:${FCREPO_DB_PORT}" -t 60 -- "$@"
