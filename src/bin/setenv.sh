JAVA_OPTS="$JAVA_OPTS \
	-Dfile.encoding=UTF-8 \
	-Dfcrepo.home=$FCREPO_HOME \
	-Dfcrepo.log=$FCREPO_LOG_LEVEL \
	-Dfcrepo.modeshape.configuration=classpath:/config/jdbc-postgresql/repository.json \
	-Dfcrepo.postgresql.host=$FCREPO_DB_HOST \
	-Dfcrepo.postgresql.port=$FCREPO_DB_PORT \
	-Dfcrepo.postgresql.username=$FCREPO_DB_USER \
	-Dfcrepo.postgresql.password=$FCREPO_DB_PASSWORD \
	-Dfcrepo.binary.directory=$FCREPO_BINARY_DIRECTORY"

# See https://wiki.lyrasis.org/display/FEDORA475/Java+HotSpot+VM+Options+recommendations
CATALINA_OPTS="$CATALINA_OPTS $FCREPO_JVM_OPTS"
