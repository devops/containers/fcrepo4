ARG jvm_version="8"

FROM tomcat:9-jre${jvm_version}

SHELL ["/bin/bash", "-c"]

#
# These args are here for RDR, which transitioned
# from a non-containerized stack in which the
# Tomcat user was uid/gid 53 and owned all the
# repository content files.
#
ARG tomcat_gid
ARG tomcat_uid

#
# See https://hub.docker.com/_/tomcat.
# CATALINA_HOME=/usr/local/tomcat
# CATALINA_HOME/bin is in PATH.
#
ENV FCREPO_BINARY_DIRECTORY=/data/fcrepo.binary.directory \
	FCREPO_DB_HOST=db \
	FCREPO_DB_NAME=fcrepo \
	FCREPO_DB_PASSWORD=fcrepo \
	FCREPO_DB_PORT=5432 \
	FCREPO_DB_USER=fcrepo \
	FCREPO_HOME=/data/fcrepo.home \
	FCREPO_JVM_OPTS="-Xms1024m -Xmx2048m -XX:MaxMetaspaceSize=512m" \
	FCREPO_LOG_LEVEL=INFO \
	LANG=en_US.UTF-8 \
	LANGUAGE=en_US:en \
	TOMCAT_GID=$tomcat_gid \
	TOMCAT_UID=$tomcat_uid \
	TZ=US/Eastern

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install \
	  curl \
	  less \
	  locales \
	  procps \
	  unzip \
	  wait-for-it \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/* ; \
	echo "$LANG UTF-8" >> /etc/locale.gen ; \
	locale-gen $LANG

WORKDIR /tmp
RUN set -eux; \
	curl -sO https://automation.lib.duke.edu/fcrepo4/4.7.5/fcrepo-webapp-4.7.5.war; \
	curl -s https://automation.lib.duke.edu/fcrepo4/4.7.5/fcrepo-webapp-4.7.5.war.sha512 \
	  | sha512sum -c - ; \
	unzip -d $CATALINA_HOME/webapps/fcrepo fcrepo-webapp-4.7.5.war; \
	rm fcrepo-webapp-4.7.5.war

WORKDIR /usr/local/bin
RUN set -eux; \
	curl -sO https://automation.lib.duke.edu/fcrepo-import-export/fcrepo-import-export-0.3.0.jar; \
        curl -s https://automation.lib.duke.edu/fcrepo-import-export/fcrepo-import-export-0.3.0.jar.sha512 \
          | sha512sum -c -

WORKDIR $CATALINA_HOME
COPY ./ ./
RUN set-eux; \
        chmod +x ./bin/docker-entrypoint.sh; \
	groupadd -g $TOMCAT_GID tomcat; \
	useradd -u $TOMCAT_UID -d $CATALINA_HOME \
	  -s /usr/sbin/nologin -g $TOMCAT_GID tomcat; \
	mv ./webapps.dist/manager ./webapps/; \
	mkdir -p /data; \
	chown ${TOMCAT_UID}:${TOMCAT_GID} /data

VOLUME /data

HEALTHCHECK CMD curl -sfI http://localhost:8080/fcrepo/rest

USER $TOMCAT_UID

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["catalina.sh", "run"]
