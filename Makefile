SHELL = /bin/bash

build_tag ?= fcrepo4

# v2: Default uid/gid changed from 53 to 8080
tomcat_uid ?= 8080
tomcat_gid ?= 8080

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) \
		--build-arg tomcat_uid=$(tomcat_uid) \
		--build-arg tomcat_gid=$(tomcat_gid) \
		./src

.PHONY : clean
clean:
	echo 'no-op'

.PHONY: test
test:
	./test.sh
