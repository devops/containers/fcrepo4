v2

	- Removed custom tomcat-users.xml file.
	- Default tomcat uid/gid changed from 53 to 8080.

v1

	- First tagged version.
