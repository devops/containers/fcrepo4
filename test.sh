#!/bin/bash

code=1
container="fcrepo4-${CI_COMMIT_SHORT_SHA:-fcrepo}"

docker-compose up -d

timeout=60
interval=5

SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $container)
    echo $status

    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

docker-compose down

exit $code
